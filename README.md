### Install and start

> Install

```bash
npm install
```

<br>

> Run

```bash
npm run start
```