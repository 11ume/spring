module.exports = {
    root: true,
    parserOptions: {
        ecmaFeatures: {
            "jsx": true
        }
    },
    ignorePatterns: [
        "node_modules/"
        , "bin/"
    ],
    extends: [
        "standard",
        "plugin:react/recommended"
    ],
    settings: {
        react: {
            version: "detect"
        }
    },
    rules: {
        "no-prototype-builtins": 0,
        "no-async-promise-executor": 0,
        "no-shadow": "error",
        "prefer-const": "error",
        "no-param-reassign": "error",
        "no-console": "error",
        "space-before-function-paren": 0,
        "array-bracket-spacing": "error",
        "object-property-newline": "error",
        "object-curly-spacing": 1,
        "object-curly-newline": [
            "error",
            {
                "ObjectExpression": {
                    "minProperties": 1
                },
                "ObjectPattern": {
                    "multiline": true
                },
                "ImportDeclaration": "never",
                "ExportDeclaration": {
                    "multiline": true
                    , "minProperties": 3
                }
            }
        ],
        "no-multiple-empty-lines": [
            "error",
            {
                "max": 1
            }
        ],
        "indent": [
            "error",
            4,
            {
                "ObjectExpression": 1,
                "SwitchCase": 1
            }
        ],
        "comma-style": [
            "error",
            "first"
        ],
        "semi": [
            "error",
            "never"
        ]
    }
}