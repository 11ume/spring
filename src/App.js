import React, { useState, useEffect } from 'react'
import { animated, useTransition } from 'react-spring'
import './App.css'

function App() {
    const [items, addItem] = useState([])

    const transitions = useTransition(items, item => item.id, {
        from: {
            transform: 'translate3d(0,-40px,0)'
        }
        , enter: {
            transform: 'translate3d(0,0px,0)'
        }
    })

    // useEffect(() => {
    //     let i = 0
    //     setInterval(() => {
    //         i++
    //         addItem({
    //             id: i
    //             , text: i
    //         })
    //     }, 2000)
    // }, [])

    return (
        <div className="App">
            <ul className="items">
                {
                    transitions.map(({ item, props, key }) => {
                        return (
                            <animated.li key={key} style={props}>
                                <p>{item.text}</p>
                            </animated.li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default App
